﻿#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED
float3 GetIncomingLight(LitSurface litSurface, Light light)
{
	return  dot(litSurface.normal, light.direction) * light.color * litSurface.color.rgb*light.attenuation;
}
//content here...
float3 GetLighting(LitSurface litSurface)
{
	float3 finalColor = float3(0.0f, 0.0f, 0.0f);

	for (int i = 0;i < _DirectionalLightCount;i++)
	{
		if (i > MAX_DIRECTIONAL_LIGHT_COUNT)break;
		finalColor += GetIncomingLight(litSurface, GetDirectionalLight(i));
	}
	for (int j = 0; j < _PointLightCount;j++)
	{
		if (j > MAX_POINT_LIGHT_COUNT)break;
		finalColor += GetIncomingLight(litSurface, GetPointLight(j,litSurface));
	}
	for (int k = 0; k < _SpotLightCount;k++)
	{
		if (k > MAX_SPOT_LIGHT_COUNT)break;
		finalColor += GetIncomingLight(litSurface, GetSpotLight(k, litSurface));
	}
	return finalColor;
}
float3 GetLighting(LitSurface litSurface, Light light)
{
	return GetIncomingLight(litSurface, light);
}
#endif