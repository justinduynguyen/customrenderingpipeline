﻿#ifndef CUSTOM_LIT_PASS_INCLUDED
#define CUSTOM_LIT_PASS_INCLUDED
#include "../UtilityShaderFunction.hlsl"
#include "LitSurfaceProperties.hlsl"
#include "LightProperties.hlsl"
#include "Lighting.hlsl"
//content here...
struct VertexAttributes {
	float3 positionOS : POSITION;
	float2 mainUV  : TEXCOORD0;
	float3 normalOS: NORMAL;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
struct Varyings {
	float4 positionCS: SV_POSITION;
	float3 positionWS :VAR_WORLD_POS;
	float2 mainUV : VAR_MAIN_UV;
	float3 normalWS :VAR_MY_NORMAL;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
UNITY_DEFINE_INSTANCED_PROP(float4, _MainTex_ST)
UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor)
UNITY_DEFINE_INSTANCED_PROP(float, _Index)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
Texture2DArray _Textures : register(t0);
SamplerState sampler_linear_repeat;
Varyings LitVertexShader(VertexAttributes input)
{
	UNITY_SETUP_INSTANCE_ID(input);

	Varyings output;
	output.normalWS = TransformObjectToWorldNormal(input.normalOS);
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	float4 mainST = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _MainTex_ST);
	output.mainUV = input.mainUV * mainST.xy + mainST.zw;
	float3 positionWorld = TransformObjectToWorld(input.positionOS);
	output.positionWS = positionWorld;
	output.positionCS = TransformWorldToHClip(positionWorld);
	return output;
}
float4 LitFragmentShader(Varyings input) : SV_TARGET
{
	UNITY_SETUP_INSTANCE_ID(input);
// Calculate lighting
LitSurface litSurface;
litSurface.color = float4(1.0f, 1.0f, 1.0f,1.0f);
litSurface.normal = input.normalWS;
litSurface.position = input.positionWS;
float3 lightColor = GetLighting(litSurface);

float4 mainTex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.mainUV);
float4 baseColor = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseColor);
float  index = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Index);
float4 arrayTex = _Textures.Sample(sampler_linear_repeat, float3(input.mainUV, index)) * baseColor;
float4 finalColor = float4(lightColor, 1) * arrayTex;
return finalColor;
}
#endif