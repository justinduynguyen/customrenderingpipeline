﻿Shader "Custom Render/CustomShaderLit"
{
	Properties
	{
		_BaseColor("Color",Color) = (1,1,1,1)
		_MainTex("_MainTex",2D) = "white"
		_Textures("Textures", 2DArray) = "" {}
		_Index("Index",Float) = 0.0
	}
		SubShader
		{
			Pass
			{
				Tags{ "LightMode" = "MyLit"}
				HLSLPROGRAM
				#pragma multi_compile_instancing
				#pragma vertex LitVertexShader
				#pragma fragment LitFragmentShader
				#include "../Lit Shaders/LitShaderPass.hlsl"
				ENDHLSL
			}
		}
}