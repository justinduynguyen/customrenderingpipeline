﻿#ifndef CUSTOM_LIT_SURFACE_PROPERTIES_INCLUDED
#define CUSTOM_LIT_SURFACE_PROPERTIES_INCLUDED

//content here...
struct LitSurface
{
	float3 normal;
	float4 color;
	float3 position;

};
#endif