﻿#ifndef CUSTOM_LIGHT_PROPERITES_INCLUDED
#define CUSTOM_LIGHT_PROPERITES_INCLUDED
#define MAX_DIRECTIONAL_LIGHT_COUNT 5
#define MAX_POINT_LIGHT_COUNT 32
#define MAX_SPOT_LIGHT_COUNT 32
CBUFFER_START(_LightDataBuffer)
//store directional light data
int _DirectionalLightCount;
float4 _DirectionalLightColors[MAX_DIRECTIONAL_LIGHT_COUNT];
float4 _DirectionalLightDirs[MAX_DIRECTIONAL_LIGHT_COUNT];
// store point light data
int _PointLightCount;
float4 _PointLightColors[MAX_POINT_LIGHT_COUNT];
float4 _PointLightPositions[MAX_POINT_LIGHT_COUNT];
// store spot light data
int _SpotLightCount;
float4 _SpotLightColors[MAX_SPOT_LIGHT_COUNT];
float4 _SpotLightPositions[MAX_SPOT_LIGHT_COUNT];
float4 _SpotLightDirections[MAX_SPOT_LIGHT_COUNT];
float4 _SpotLightAngles[MAX_SPOT_LIGHT_COUNT];
CBUFFER_END
//content here...
struct Light
{
	float3 color;
	float3 direction;
	float attenuation;
};
int GetDirectionalLightCount() {
	return _DirectionalLightCount;
}
Light GetDirectionalLight()
{
	Light light;
	light.color = float3(1, 1, 1);
	light.direction = float3(1, 0, 1);
	return light;
}
Light GetDirectionalLight(int index)
{
	Light light;
	light.color = _DirectionalLightColors[index].rgb;
	light.direction = _DirectionalLightDirs[index].xyz;
	light.attenuation = 1.0;
	return light;
}
Light GetPointLight(int index, LitSurface litSurface)
{
	Light light;
	light.color = _PointLightColors[index].rgb;
	float3 distance =  _PointLightPositions[index].xyz- litSurface.position;
	light.direction = normalize(distance);
	float squaredDistance = max(dot(distance, distance), 0.00001);
	float squaredLightRange = 1/ pow(max(_PointLightPositions[index].w, 0.00001), 2);
	float fadeOutAttenuation = max(0,1-pow( squaredDistance * squaredLightRange,2));
	fadeOutAttenuation *= fadeOutAttenuation;
	light.attenuation = fadeOutAttenuation/ squaredDistance;
	return light;
}
Light GetSpotLight(int index, LitSurface litSurface)
{
	Light light;
	light.color = _SpotLightColors[index].rgb;
	float3 distance = _SpotLightPositions[index].xyz - litSurface.position;
	light.direction = normalize(distance);
	float squaredDistance = max(dot(distance, distance), 0.00001);
	float squaredLightRange = 1 / pow(max(_SpotLightPositions[index].w, 0.00001), 2);
	// fade out in range
	float fadeOutAttenuation = pow(max(0, 1 - pow(squaredDistance * squaredLightRange, 2)),2);

	//fade out in spot range
	float inner = cos(0.5 * _SpotLightAngles[index].x);
	float outter = cos(0.5 * _SpotLightAngles[index].y);
	float a = 1 / max(inner - outter, 0.0001f);
	float b = -outter * a;
	float spotAttenuation = pow(saturate(dot(_SpotLightDirections[index].xyz, light.direction) * a + b), 2);


	light.attenuation = spotAttenuation*fadeOutAttenuation/ squaredDistance;
	return light;
}
#endif