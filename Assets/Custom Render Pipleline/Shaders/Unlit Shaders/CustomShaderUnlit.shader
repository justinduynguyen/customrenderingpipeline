﻿Shader "Custom Render/CustomShaderUnlit"
{
    Properties
    {
        _BaseColor("Color",Color) = (1,1,1,1)
        _MainTex("_MainTex",2D)="white"
        _Textures("Textures", 2DArray) = "" {}
        _Index ("Index",Float)=0.0
    }
        SubShader
    {
        Pass
        {
            HLSLPROGRAM
            #pragma multi_compile_instancing
            #pragma vertex UnlitVertexShader
            #pragma fragment UnlitFragmentShader           
            #include "../Unlit Shaders/UnlitShaderPass.hlsl"
            ENDHLSL
        }
    }
}
