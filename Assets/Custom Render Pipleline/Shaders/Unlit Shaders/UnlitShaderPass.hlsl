﻿#ifndef CUSTOM_UNLIT_PASS_INCLUDED
#define CUSTOM_UNLIT_PASS_INCLUDED
#include "../UtilityShaderFunction.hlsl"
//content here...
struct VertexAttributes {
	float3 positionOS : POSITION;
	float2 mainUV  : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
struct Varyings {
	float4 positionCS: SV_POSITION;
	float2 mainUV : VAR_MAIN_UV;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
	UNITY_DEFINE_INSTANCED_PROP(float4, _MainTex_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor)
	UNITY_DEFINE_INSTANCED_PROP(float,_Index)
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
Texture2DArray _Textures : register(t0);
SamplerState sampler_linear_repeat;
Varyings UnlitVertexShader(VertexAttributes input)
{ 
	UNITY_SETUP_INSTANCE_ID(input);
	Varyings output;
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	float4 mainST = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _MainTex_ST);
	output.mainUV = input.mainUV * mainST.xy + mainST.zw;
    float3 positionWorld = TransformObjectToWorld(input.positionOS);
	output.positionCS = TransformWorldToHClip(positionWorld);
	return output;
}
float4 UnlitFragmentShader(Varyings input) : SV_TARGET
{
	UNITY_SETUP_INSTANCE_ID(input);
    float4 mainTex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.mainUV);
	float4 baseColor = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _BaseColor);
	float  index = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial, _Index);
	float4 arrayTex = _Textures.Sample(sampler_linear_repeat, float3(input.mainUV, index))*baseColor;
    return arrayTex;
}
#endif