﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public partial class CameraRenderer
{
    private static ShaderTagId[] builtInShaderTags = new ShaderTagId[] {
        new ShaderTagId("SRPDefaultUnlit"),
        new ShaderTagId("Always"),
        new ShaderTagId("ForwardBase"),
        new ShaderTagId("ForwardAdd"),
        new ShaderTagId("Vertex"),
        new ShaderTagId("VertexLWRGBM"),
        new ShaderTagId("VertexLM")
    };

    private static ShaderTagId myLitTagId = new ShaderTagId("MyLit");
    private ScriptableRenderContext _context;
    private Camera _camera;
    private CullingResults cullingResults = new CullingResults();
    private CommandBuffer commandBuffer = new CommandBuffer() { name = "MainCamera" };
    private string bufferName;

    private Lightinger lightinger = new Lightinger();

    public void Render(ScriptableRenderContext context, Camera camera, bool enableGPUInstancing,bool enableDynamicBatching)
    {
        _context = context;
        _camera = camera;
        DrawGizmos();
        SetupPreExecute();
        if (!Culling()) return;
        lightinger.ExecuteSetupGPUData(_context, cullingResults);
        DrawGeometry(enableGPUInstancing, enableDynamicBatching);
        SubmitToGPU();
    }

    private void DrawGeometry(bool enableGPUInstancing,bool enableDynamicBatching)
    {
        DrawSkybox();
        DrawOpaqueGeometry(enableGPUInstancing, enableDynamicBatching);
        DrawTransparentGeometry(enableGPUInstancing, enableDynamicBatching);
    }

    private void DrawOpaqueGeometry(bool enableGPUInstancing,bool enableDynamicBatching)
    {
        var sortingSettings = new SortingSettings(_camera) { criteria = SortingCriteria.CommonOpaque };
        var drawSettings = new DrawingSettings(builtInShaderTags[0], sortingSettings)
        {
            enableInstancing = enableGPUInstancing, enableDynamicBatching = enableDynamicBatching
        };
        drawSettings.SetShaderPassName(1, myLitTagId);
        var filterSettings = new FilteringSettings(RenderQueueRange.opaque);
        _context.DrawRenderers(cullingResults, ref drawSettings, ref filterSettings);
    }

    private void DrawTransparentGeometry(bool enableGPUInstancing,bool enableDynamicBatching)
    {
        var sortingSettings = new SortingSettings(_camera) { criteria = SortingCriteria.CommonTransparent };
        var drawSettings = new DrawingSettings(builtInShaderTags[0], sortingSettings) { enableInstancing = enableGPUInstancing ,enableDynamicBatching = enableDynamicBatching};
        drawSettings.SetShaderPassName(1, myLitTagId);
        var filterSettings = new FilteringSettings(RenderQueueRange.transparent);
        _context.DrawRenderers(cullingResults, ref drawSettings, ref filterSettings);
    }

    private void DrawSkybox()
    {
        _context.DrawSkybox(_camera);
    }

    private bool Culling()
    {
        if (_camera.TryGetCullingParameters(out ScriptableCullingParameters p))
        {
            cullingResults = _context.Cull(ref p);
            return true;
        }
        return false;
    }

    private void SubmitToGPU()
    {
        commandBuffer.EndSample(bufferName);
        _context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();
        _context.Submit();
    }

    private void SetupPreExecute()
    {
        bufferName = _camera.name;
        commandBuffer.name = bufferName;
        _context.SetupCameraProperties(_camera);
        bool clearDepth = _camera.clearFlags <= CameraClearFlags.Depth;
        bool clearColor = _camera.clearFlags == CameraClearFlags.Color;
        Color color = clearColor == true ? _camera.backgroundColor.linear : Color.clear;
        commandBuffer.ClearRenderTarget(clearDepth, clearColor, color);

        commandBuffer.BeginSample(bufferName);
        _context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();
    }

    partial void DrawGizmos();

#if UNITY_EDITOR

    partial void DrawGizmos()
    {
        if (Handles.ShouldRenderGizmos())
        {
            _context.DrawGizmos(_camera, GizmoSubset.PreImageEffects);
            _context.DrawGizmos(_camera, GizmoSubset.PostImageEffects);
        }
        if (_camera.cameraType == CameraType.SceneView)
        {
            ScriptableRenderContext.EmitWorldGeometryForSceneView(_camera);
        }
    }

#endif
}