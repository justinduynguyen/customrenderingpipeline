﻿using UnityEngine;
using UnityEngine.Rendering;

public class MyCustomRenderPipeline : RenderPipeline
{
    private bool enableGPUInstancing, enableDynamicBatching;

    public MyCustomRenderPipeline(bool enableSPRBatcher, bool enableGPUInstancing, bool enableDynamicBatching)
    {
        GraphicsSettings.useScriptableRenderPipelineBatching = enableSPRBatcher;
        GraphicsSettings.lightsUseLinearIntensity = true;
        this.enableGPUInstancing = enableGPUInstancing;
        this.enableDynamicBatching = enableDynamicBatching;
    }

    private CameraRenderer cameraRenderer = new CameraRenderer();

    protected override void Render(ScriptableRenderContext context, Camera[] cameras)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            cameraRenderer.Render(context, cameras[i], enableGPUInstancing,enableDynamicBatching);
        }
    }
}