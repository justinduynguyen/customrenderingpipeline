﻿using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public class Lightinger
{
    public static int maxDirectionalLightCount = 5;
    public static int maxPointLightCount = 32;
    public static int maxSpotLightCount = 32;
    private static string bufferName = "Lightinger";

    // directional light
    private static int directionalLightColorId = Shader.PropertyToID("_DirectionalLightColors");
    private static int directionalLightDirectionId = Shader.PropertyToID("_DirectionalLightDirs");
    private static int directionalLightCountId = Shader.PropertyToID("_DirectionalLightCount");
    private static Vector4[] dirLightColors = new Vector4[maxDirectionalLightCount];
    private static Vector4[] dirLightDirections = new Vector4[maxDirectionalLightCount];

    // point light
    private static int pointLightColorId = Shader.PropertyToID("_PointLightColors");
    private static int pointLightPositionId = Shader.PropertyToID("_PointLightPositions");
    private static int pointLightCountId = Shader.PropertyToID("_PointLightCount");
    private static Vector4[] pointLightColors = new Vector4[maxPointLightCount];
    private static Vector4[] pointLightPositions = new Vector4[maxPointLightCount];

    // spot light
    private static int spotLightColorId = Shader.PropertyToID("_SpotLightColors");
    private static int spotLightPositionId = Shader.PropertyToID("_SpotLightPositions");
    private static int spotLightDirectionId = Shader.PropertyToID("_SpotLightDirections");
    private static int spotLightAngleId = Shader.PropertyToID("_SpotLightAngles");
    private static int spotLightCountId = Shader.PropertyToID("_SpotLightCount");
    private static Vector4[] spotLightColors = new Vector4[maxSpotLightCount];
    private static Vector4[] spotLightPositions = new Vector4[maxSpotLightCount];
    private static Vector4[] spotLightAngles = new Vector4[maxSpotLightCount];
    private static Vector4[] spotLightDirections = new Vector4[maxSpotLightCount];
    private CullingResults cullingResults;


    private CommandBuffer commandBuffer = new CommandBuffer()
    {
        name = bufferName
    };

    private void SendDirectionalLightDataToGPU()
    {
        NativeArray<VisibleLight> visibleLights = cullingResults.visibleLights;
        int count = 0;
        for (int i = 0; i < visibleLights.Length; i++)
        {
            if (count > maxDirectionalLightCount) break;
            if (visibleLights[i].lightType == LightType.Directional)
            {
                SendDirectionLightDataToArray(count, visibleLights[i]);
                count++;
            }
        }

        commandBuffer.SetGlobalInt(directionalLightCountId, count);
        commandBuffer.SetGlobalVectorArray(directionalLightDirectionId, dirLightDirections);
        commandBuffer.SetGlobalVectorArray(directionalLightColorId, dirLightColors);
    }

    private void SendPointLightDataToGPU()
    {
        NativeArray<VisibleLight> visibleLights = cullingResults.visibleLights;
        int count = 0;
        for (int i = 0; i < visibleLights.Length; i++)
        {
            if (count > maxPointLightCount) break;
            if (visibleLights[i].lightType == LightType.Point)
            {
                SendPointLightDataToArray(count, visibleLights[i]);
                count++;
            }
        }

        commandBuffer.SetGlobalInt(pointLightCountId, count);
        commandBuffer.SetGlobalVectorArray(pointLightPositionId, pointLightPositions);
        commandBuffer.SetGlobalVectorArray(pointLightColorId, pointLightColors);
    }
    private void SendSpotLightDataToGPU()
    {
        NativeArray<VisibleLight> visibleLights = cullingResults.visibleLights;
        int count = 0;
        for (int i = 0; i < visibleLights.Length; i++)
        {
            if (count > maxSpotLightCount) break;
            if (visibleLights[i].lightType == LightType.Spot)
            {
                SendSpotLightDataToArray(count, visibleLights[i]);
                count++;
            }
        }

        commandBuffer.SetGlobalInt(spotLightCountId, count);
        commandBuffer.SetGlobalVectorArray(spotLightPositionId, spotLightPositions);
        commandBuffer.SetGlobalVectorArray(spotLightColorId, spotLightColors);
        commandBuffer.SetGlobalVectorArray(spotLightDirectionId, spotLightDirections);
        commandBuffer.SetGlobalVectorArray(spotLightAngleId, spotLightAngles);
    }
    
    private void SendSpotLightDataToArray(int index, VisibleLight visibleLight)
    {
        spotLightColors[index] = visibleLight.finalColor;
        spotLightPositions[index] = visibleLight.localToWorldMatrix.GetColumn(3);
        spotLightPositions[index].w = visibleLight.range;
        spotLightAngles[index] = new Vector4(Mathf.Deg2Rad*visibleLight.light.innerSpotAngle, Mathf.Deg2Rad*visibleLight.spotAngle);
        spotLightDirections[index] = -visibleLight.localToWorldMatrix.GetColumn(2);
    }

    private void SendPointLightDataToArray(int index, VisibleLight visibleLight)
    {
        pointLightColors[index] = visibleLight.finalColor;     
        pointLightPositions[index] = visibleLight.localToWorldMatrix.GetColumn(3);
        pointLightPositions[index].w = visibleLight.range;
        
      
    }

    private void SendDirectionLightDataToArray(int index, VisibleLight visibleLight)
    {
        dirLightColors[index] = visibleLight.finalColor;
        dirLightDirections[index] = -visibleLight.localToWorldMatrix.GetColumn(2);
    }

    public void ExecuteSetupGPUData(ScriptableRenderContext context, CullingResults cullingResults)
    {
        this.cullingResults = cullingResults;
        // Debug.Log(cullingResults.visibleLights.Length);
        commandBuffer.BeginSample(bufferName);
        SendDirectionalLightDataToGPU();
        SendPointLightDataToGPU();
        SendSpotLightDataToGPU();
        commandBuffer.EndSample(bufferName);
        context.ExecuteCommandBuffer(commandBuffer);
        commandBuffer.Clear();
    }
}