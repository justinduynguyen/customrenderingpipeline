﻿using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu(menuName = "Rendering/Create render pipeline asset")]
public class CustomRenderPipelineAsset : RenderPipelineAsset
{
    public bool enableSRPBatcher = false;
    public bool enableGPUInstancing = false;
    public bool enableDynamicBatching = false;
    protected override RenderPipeline CreatePipeline()
    {
        return new MyCustomRenderPipeline(enableSRPBatcher, enableGPUInstancing,enableDynamicBatching);
    }
}