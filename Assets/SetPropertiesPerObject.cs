﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[DisallowMultipleComponent]
public class SetPropertiesPerObject : MonoBehaviour
{
    static int _baseColorID = Shader.PropertyToID("_BaseColor");
    static int _indexID = Shader.PropertyToID("_Index");
    static MaterialPropertyBlock _block;
    [SerializeField]
    Color baseColor;
    [SerializeField]
    float index;
    private void Awake()
    {
        OnValidate();
    }
    private void OnValidate()
    {
        if (_block == null) _block = new MaterialPropertyBlock();

        _block.SetColor(_baseColorID, baseColor);
        _block.SetFloat(_indexID, index);
        GetComponent<Renderer>().SetPropertyBlock(_block);
    }
}
