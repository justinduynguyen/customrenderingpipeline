﻿using UnityEditor;
using UnityEngine;
[CustomEditorForRenderPipeline(typeof(Light),typeof(CustomRenderPipelineAsset))]
public class LightingConfigEditor : LightEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		if (
			!settings.lightType.hasMultipleDifferentValues &&
			(LightType)settings.lightType.enumValueIndex == LightType.Spot
		)
		{
			settings.DrawInnerAndOuterSpotAngle();
			settings.ApplyModifiedProperties();
		}
	}
}
